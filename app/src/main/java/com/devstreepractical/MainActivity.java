package com.devstreepractical;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.devstreepractical.adapter.LocationListAdapter;
import com.devstreepractical.databinding.ActivityMainBinding;
import com.devstreepractical.db.DatabaseHandler;
import com.devstreepractical.interfaces.onItemClickListener;
import com.devstreepractical.model.LocData;
import com.devstreepractical.utils.AppConstant;
import com.devstreepractical.utils.SpacesItemDecoration;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity implements onItemClickListener {

    ActivityMainBinding binding;

    DatabaseHandler db;
    LocationListAdapter adapter;
    ArrayList<LocData> locData = new ArrayList<>();
    String sort = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        db = new DatabaseHandler(this);

        binding.topAppBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_asc) {
                    sort = "asc";
                    sortAsc();

                } else if (item.getItemId() == R.id.menu_desc) {
                    sort = "desc";
                    sortDesc();

                } else if (item.getItemId() == R.id.menu_path) {
                    if (locData != null && locData.size() > 0) {
                        Intent mIntent = new Intent(MainActivity.this, DrawPathMapActivity.class);
                        startActivity(mIntent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Please add locations to view on map!", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        binding.floatAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(MainActivity.this, SearchLocationActivity.class);
                startActivity(mIntent);
            }
        });

//        if (checkPermission()) {
//            getCurrentLocationForeground();
//        }

        adapter = new LocationListAdapter(locData, this);
        binding.rvLocationList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        binding.rvLocationList.addItemDecoration(new SpacesItemDecoration(20));

        readLocations();


    }


    private void sortAsc() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locData.sort(LocData.ascSorting);
        } else {
            Collections.sort(locData, new Comparator<LocData>() {
                @Override
                public int compare(LocData place1, LocData place2) {
                    double lat1 = place1.getLatitude();
                    double lon1 = place1.getLongitude();
                    double lat2 = place2.getLatitude();
                    double lon2 = place2.getLongitude();

                    double distanceToPlace1 = LocData.distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat1, lon1);
                    double distanceToPlace2 = LocData.distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat2, lon2);
                    String log = "------- readLocations Places after sorting distanceToPlace1: " + distanceToPlace1 + " city: " + place1.getLocationName() + " distanceToPlace2: " + distanceToPlace2 + " city: " + place2.getLocationName();
                    Log.e("MainActivity ", log);
                    return (int) (distanceToPlace1 - distanceToPlace2);
                }

            });
        }

        for (LocData ln : locData) {
            String log = "readLocations Places after sorting Id: " + ln.getID() + " ,Location Name: " + ln.getLocationName() + " ,Lat: " +
                    ln.getLatitude() + " ,Lng: " + ln.getLongitude();
            // Writing Contacts to log
            Log.e("MainActivity ", log);
        }
        adapter.notifyDataSetChanged();

    }

    private void sortDesc() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locData.sort(LocData.descSorting);
        } else {
            Collections.sort(locData, new Comparator<LocData>() {
                @Override
                public int compare(LocData place1, LocData place2) {
                    double lat1 = place1.getLatitude();
                    double lon1 = place1.getLongitude();
                    double lat2 = place2.getLatitude();
                    double lon2 = place2.getLongitude();

                    double distanceToPlace1 = LocData.distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat1, lon1);
                    double distanceToPlace2 = LocData.distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat2, lon2);
                    String log = "------- readLocations Places after sorting distanceToPlace1: " + distanceToPlace1 + " city: " + place1.getLocationName() + " distanceToPlace2: " + distanceToPlace2 + " city: " + place2.getLocationName();
                    Log.e("MainActivity ", log);

                    return (int) (distanceToPlace2 - distanceToPlace1);
                }

            });
        }

        for (LocData ln : locData) {
            String log = "------- readLocations Places after sorting Id: " + ln.getID() + " ,Location Name: " + ln.getLocationName() + " ,Lat: " +
                    ln.getLatitude() + " ,Lng: " + ln.getLongitude();
            // Writing Contacts to log
            Log.e("MainActivity ", log);
        }
        adapter.notifyDataSetChanged();
    }


    private void readLocations() {
        locData.clear();
        Log.d("Reading: ", "Reading all contacts..");
        locData = db.getAllLocations();

        for (LocData ln : locData) {
            String log = "readLocations Id: " + ln.getID() + " ,Location Name: " + ln.getLocationName() + " ,Lat: " +
                    ln.getLatitude() + " ,Lng: " + ln.getLongitude();
            // Writing Contacts to log
            Log.e("MainActivity ", log);
        }

        adapter = new LocationListAdapter(locData, this);
        binding.rvLocationList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        binding.rvLocationList.setAdapter(adapter);

        if (locData != null && locData.size() > 0) {
            AppConstant.firstLocation = new LatLng(locData.get(0).getLatitude(), locData.get(0).getLongitude());
        }

        if (sort.contains("asc")) {
            sortAsc();
        } else if (sort.contains("desc")) {
            sortDesc();
        }
    }

    @Override
    public void onItemEditClick(String key, int position) {
        if (key.equalsIgnoreCase("edit")) {
            Intent mIntent = new Intent(MainActivity.this, SearchLocationActivity.class);
            mIntent.putExtra("locData", locData.get(position));
            startActivity(mIntent);
        } else if (key.equalsIgnoreCase("delete")) {
            db.deleteLocation(locData.get(position));
            locData.remove(position);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "Location removed successfully", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        readLocations();
    }

}