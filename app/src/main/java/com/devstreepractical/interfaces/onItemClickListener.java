package com.devstreepractical.interfaces;

public interface onItemClickListener {
    void onItemEditClick(String key, int position);
}
