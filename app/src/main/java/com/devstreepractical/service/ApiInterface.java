package com.devstreepractical.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;


public interface ApiInterface {

    @Headers("Cache-Control: no-cache")
    @GET("maps/api/directions/json?")
    Call<ResponseBody> getPaths(
            @Query("origin") String origin,
            @Query("destination") String destination,
            @Query("key") String key
    );


}
