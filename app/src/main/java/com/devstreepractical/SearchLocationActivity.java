package com.devstreepractical;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.devstreepractical.databinding.ActivitySearchLocationBinding;
import com.devstreepractical.db.DatabaseHandler;
import com.devstreepractical.model.LocData;
import com.devstreepractical.utils.AppConstant;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.util.Arrays;

public class SearchLocationActivity extends AppCompatActivity {

    ActivitySearchLocationBinding binding;
    LocData mLocDataReceived;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySearchLocationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
//        setContentView(R.layout.activity_search_location);
        DatabaseHandler db = new DatabaseHandler(this);
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);


        if (getIntent().getExtras() != null) {
            mLocDataReceived = (LocData) getIntent().getSerializableExtra("locData");
            autocompleteFragment.setText(mLocDataReceived.getLocationName());
        }

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), AppConstant.API_KEY);
        }


        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));
        autocompleteFragment.setTypeFilter(TypeFilter.CITIES);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                Log.i("TAG", "Place: " + place.getName() + ", " + place.getId() + " LAt:" + place.getLatLng());
                if (mLocDataReceived == null) {
                    db.addLocationData(new LocData(place.getName(), place.getLatLng().latitude, place.getLatLng().longitude));
                    Toast.makeText(getApplicationContext(), place.getName() + " is Added", Toast.LENGTH_SHORT).show();
                } else {

                    mLocDataReceived.setLocationName(place.getName());
                    mLocDataReceived.setLatitude(place.getLatLng().latitude);
                    mLocDataReceived.setLongitude(place.getLatLng().longitude);
                    db.updateLocation(mLocDataReceived);
                    Toast.makeText(getApplicationContext(), place.getName() + " is updated", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onError(@NonNull Status status) {
                Log.i("TAG", "An error occurred: " + status);
            }
        });


    }
}