package com.devstreepractical;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.devstreepractical.db.DatabaseHandler;
import com.devstreepractical.model.LocData;
import com.devstreepractical.service.ApiClient;
import com.devstreepractical.service.ApiInterface;
import com.devstreepractical.utils.AppConstant;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DrawPathMapActivity extends AppCompatActivity {

    private GoogleMap map;
    ArrayList<LocData> locDataArrayList = new ArrayList<>();
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_path_map);

        db = new DatabaseHandler(this);
        readLocations();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                if (map != null) {
                    Marker marker = null;
                    LatLng latLng = null;
                    for (LocData ld : locDataArrayList) {
                        latLng = new LatLng(ld.getLatitude(), ld.getLongitude());
                        marker = map.addMarker(new MarkerOptions().position(latLng).title(ld.getLocationName()));
                    }
                    if (latLng != null) {
                        latLng = new LatLng(latLng.latitude, latLng.longitude);
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));
                        map.animateCamera(CameraUpdateFactory.zoomTo(7), 2000, null);
                    }
                }
            }
        });

    }

    List<String> mUrls = new ArrayList<>();

    private void readLocations() {
        locDataArrayList.clear();
        Log.d("Reading: ", "Reading all contacts..");
        locDataArrayList = db.getAllLocations();
        sortAsc();
        ArrayList<LatLng> markerPoints = new ArrayList<>();

        for (LocData cn : locDataArrayList) {
            String log = "readLocations Id: " + cn.getID() + " ,Location Name: " + cn.getLocationName() + " ,Lat: " + cn.getLatitude() + " ,Lng: " + cn.getLongitude();
            Log.e("MapPathActivity ", log);
            markerPoints.add(new LatLng(cn.getLatitude(), cn.getLongitude()));
        }
        if (locDataArrayList.size() > 2) {

            mUrls = getDirectionsUrl(markerPoints);
            for (String url : mUrls) {
                Log.e("MapPathActivity ", "URL:" + url.replace("/", "\\"));
            }

            LatLng str_origin = markerPoints.get(0);
            LatLng str_dest = markerPoints.get(1);
            drawPath(str_origin, str_dest);

            for (int i = 2; i < markerPoints.size(); i++)//loop starts from 2 because 0 and 1 are already printed
            {
                str_origin = str_dest;
                str_dest = markerPoints.get(i);
                drawPath(str_origin, str_dest);
            }

        }
    }


    private void sortAsc() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locDataArrayList.sort(LocData.ascSorting);
        } else {
            Collections.sort(locDataArrayList, new Comparator<LocData>() {
                @Override
                public int compare(LocData place1, LocData place2) {
                    double lat1 = place1.getLatitude();
                    double lon1 = place1.getLongitude();
                    double lat2 = place2.getLatitude();
                    double lon2 = place2.getLongitude();

                    double distanceToPlace1 = LocData.distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat1, lon1);
                    double distanceToPlace2 = LocData.distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat2, lon2);
                    String log = "------- readLocations Places after sorting distanceToPlace1: " + distanceToPlace1 + " city: " + place1.getLocationName() + " distanceToPlace2: " + distanceToPlace2 + " city: " + place2.getLocationName();
                    Log.e("MainActivity ", log);
                    return (int) (distanceToPlace1 - distanceToPlace2);
                }

            });
        }


    }

    private List<String> getDirectionsUrl(ArrayList<LatLng> markerPoints) {
        List<String> mUrls = new ArrayList<>();
        if (markerPoints.size() > 1) {
            String str_origin = markerPoints.get(0).latitude + "," + markerPoints.get(0).longitude;
            String str_dest = markerPoints.get(1).latitude + "," + markerPoints.get(1).longitude;

            String sensor = "&sensor=false&mode=driving&alternatives=true";
            String parameters = "?origin=" + str_origin + "&destination=" + str_dest + sensor;
            String url = "https://maps.googleapis.com/maps/api/directions/json" + parameters;
            mUrls.add(url);

            for (int i = 2; i < markerPoints.size(); i++)//loop starts from 2 because 0 and 1 are already printed
            {
                str_origin = str_dest;
                str_dest = markerPoints.get(i).latitude + "," + markerPoints.get(i).longitude;
                parameters = "?origin=" + str_origin + "&destination=" + str_dest + sensor;
                url = "https://maps.googleapis.com/maps/api/directions/json" + parameters;
                mUrls.add(url);
            }
        }

        return mUrls;
    }

    private void drawPath(LatLng source, LatLng destination) {

        String str_origin = source.latitude + "," + source.longitude;
        String str_dest = destination.latitude + "," + destination.longitude;

        ApiInterface apiInterface;
        apiInterface = ApiClient.dataResponse().create(ApiInterface.class);
        apiInterface.getPaths(str_origin, str_dest, AppConstant.API_KEY).
                enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                final JSONObject json = new JSONObject(response.body().string());
                                JSONArray routeArray = json.getJSONArray("routes");
                                JSONObject routes = routeArray.getJSONObject(0);
                                JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
                                String encodedString = overviewPolylines.getString("points");
                                List<LatLng> list = decodePoly(encodedString);
                                Polyline line = map.addPolyline(new PolylineOptions()
                                        .addAll(list)
                                        .width(12)
                                        .color(Color.parseColor("#05b1fb"))//Google maps blue color
                                        .geodesic(true)
                                );
                               /*
                               for(int z = 0; z<list.size()-1;z++){
                                    LatLng src= list.get(z);
                                    LatLng dest= list.get(z+1);
                                    Polyline line = mMap.addPolyline(new PolylineOptions()
                                    .add(new LatLng(src.latitude, src.longitude), new LatLng(dest.latitude,   dest.longitude))
                                    .width(2)
                                    .color(Color.BLUE).geodesic(true));
                                }
                               */
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
}