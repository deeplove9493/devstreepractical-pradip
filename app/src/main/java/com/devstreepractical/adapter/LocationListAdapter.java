package com.devstreepractical.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devstreepractical.R;
import com.devstreepractical.interfaces.onItemClickListener;
import com.devstreepractical.model.LocData;

import java.util.ArrayList;

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.NewUsersViewHolder> {

    private ArrayList<LocData> list;
    private onItemClickListener onClickListener;

    public LocationListAdapter( ArrayList<LocData> mList, onItemClickListener onItemClickListener) {
        this.list = mList;
        this.onClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public NewUsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new, null);
        return new NewUsersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewUsersViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (list.size() > 0) {
            holder.txtLocation.setText(list.get(holder.getAdapterPosition()).getLocationName());
        }
        holder.mImageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemEditClick("edit", holder.getAdapterPosition());
            }
        });
        holder.mImageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onItemEditClick("delete", holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class NewUsersViewHolder extends RecyclerView.ViewHolder {
        TextView txtLocation;
        ImageView mImageViewEdit;
        ImageView mImageViewDelete;

        public NewUsersViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLocation = itemView.findViewById(R.id.txtLocation);
            mImageViewEdit = itemView.findViewById(R.id.imgEdit);
            mImageViewDelete = itemView.findViewById(R.id.imgDelete);
        }
    }
}