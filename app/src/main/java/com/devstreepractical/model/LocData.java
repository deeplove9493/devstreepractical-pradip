package com.devstreepractical.model;

import android.util.Log;

import com.devstreepractical.utils.AppConstant;

import java.io.Serializable;
import java.util.Comparator;

public class LocData implements Serializable {
    int _id;
    String _location;
    Double _latitude;
    Double _longitude;

    public LocData() {
    }

    public LocData(int id, String name, Double _latitude, Double _longitude) {
        this._id = id;
        this._location = name;
        this._latitude = _latitude;
        this._longitude = _longitude;
    }

    public LocData(String name, Double _latitude, Double _longitude) {
        this._location = name;
        this._latitude = _latitude;
        this._longitude = _longitude;
    }

//    public LocData(String name, String _latitude) {
//        this._location = name;
//        this._latitude = _latitude;
//    }

    public int getID() {
        return this._id;
    }

    public void setID(int id) {
        this._id = id;
    }

    public String getLocationName() {
        return this._location;
    }

    public void setLocationName(String name) {
        this._location = name;
    }

    public Double getLatitude() {
        return this._latitude;
    }

    public void setLatitude(Double _latitude) {
        this._latitude = _latitude;
    }

    public Double getLongitude() {
        return this._longitude;
    }

    public void setLongitude(Double _longitude) {
        this._longitude = _longitude;
    }

    public static Comparator<LocData> ascSorting = new Comparator<LocData>() {
        @Override
        public int compare(LocData place1, LocData place2) {
            double lat1 = place1.getLatitude();
            double lon1 = place1.getLongitude();
            double lat2 = place2.getLatitude();
            double lon2 = place2.getLongitude();

            double distanceToPlace1 = distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat1, lon1);
            double distanceToPlace2 = distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat2, lon2);
            return (int) (distanceToPlace1 - distanceToPlace2);
        }
    };
    public static Comparator<LocData> descSorting = new Comparator<LocData>() {
        @Override
        public int compare(LocData place1, LocData place2) {
            double lat1 = place1.getLatitude();
            double lon1 = place1.getLongitude();
            double lat2 = place2.getLatitude();
            double lon2 = place2.getLongitude();

            double distanceToPlace1 = distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat1, lon1);
            double distanceToPlace2 = distance(AppConstant.firstLocation.latitude, AppConstant.firstLocation.longitude, lat2, lon2);
            return (int) (distanceToPlace2 - distanceToPlace1);
        }
    };

    public static double distance(double fromLat, double fromLong, double toLat, double toLong) {

        final double earthRadius = 6371;

        final double dLat = Math.toRadians(toLat - fromLat);
        final double dLng = Math.toRadians(toLong - fromLong);

        final double sindLat = Math.sin(dLat / 2);
        final double sindLng = Math.sin(dLng / 2);

        final double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(fromLat))
                * Math.cos(Math.toRadians(toLat));

        final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        final double dist = earthRadius * c;
        Log.i("LocData ", "Distance: " + dist);
        return dist;
    }
}